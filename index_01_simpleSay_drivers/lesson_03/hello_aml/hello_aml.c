#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>


/*
* 使用argv[1]来传入，需要打开的设备文件的路径
* 用法: #./data/hello_aml  /dev/aml_misc_dev
*/
int main(int argc, char *argv[])
{

  int buf[4]={0};
  int fd=0;

  if(argc < 2){
    printf("Usage: hello_aml  /dev/aml_misc_dev\n");
    return 0;
  }

  //(1) 测试 open
  fd = open(argv[1], O_RDWR);
  if(fd == -1){
	printf("Failed: open %s \n", argv[1]);
	return 0;
  }

  //(2) 测试 ioctl
  ioctl(fd, 0x12, buf); 

  //(3) 测试 clsoe	
  close(fd);
  return 0;

}
