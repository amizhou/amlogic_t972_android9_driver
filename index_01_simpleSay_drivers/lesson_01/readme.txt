基于Amlogic 安卓9.0, 驱动简说（一）：字符设备驱动，手动创建设备
原创博客：https://blog.csdn.net/yyzsyx/article/details/128468972

## （一）APP文件存放位置：
szhou@bc04:~/T972/android_x301/source/t962x3-t972-android9.0/development$ tree -L 1 hello_aml/
hello_aml/
├── Android.mk
└── hello_aml.c

0 directories, 2 files
szhou@bc04:~/T972/android_x301/source/t962x3-t972-android9.0/development$ 
编译：
1. # source build/envsetup.sh 
2. # lunch  your-board
3. # make hello_aml



## （二）驱动文件存放位置：
szhou@bc04:~/T972/android_x301/source/t962x3-t972-android9.0/common/drivers/amlogic/input$ tree -L 1 
.
├── aml_tool.c
├── avin_detect
├── helloworld_amlogic_char_driver.c
├── Kconfig
├── keyboard
├── Makefile
├── remote
├── sensor
└── touchscreen

5 directories, 6 files
szhou@bc04:~/T972/android_x301/source/t962x3-t972-android9.0/common/drivers/amlogic/input$ 