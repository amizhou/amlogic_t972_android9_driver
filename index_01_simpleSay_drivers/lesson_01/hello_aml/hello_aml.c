#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>



int main()
{


  int buf[4]={0};
  int fd=0;

  //(1) 测试 open
  fd = open("/dev/aml_char", O_RDWR);
  if(fd == -1){
	printf("Failed: open /dev/aml_char \n");
	return 0;
  }

  //(2) 测试 ioctl
  ioctl(fd, 0x12, buf); 

  //(3) 测试 clsoe	
  close(fd);
  return 0;

}
